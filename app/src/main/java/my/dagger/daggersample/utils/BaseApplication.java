package my.dagger.daggersample.utils;

import android.app.Application;

import my.dagger.daggersample.dagger.DaggerMyComponent;
import my.dagger.daggersample.dagger.MyModule;

public class BaseApplication extends Application {

    public static BaseApplication baseApp;

    public static DaggerMyComponent myComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        baseApp = this;

        myComponent = DaggerMyComponent.builder()
                .myModule(new MyModule(this)).build();

    }
}
