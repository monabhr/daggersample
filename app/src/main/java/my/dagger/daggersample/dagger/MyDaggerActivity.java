package my.dagger.daggersample.dagger;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import javax.inject.Inject;

import my.dagger.daggersample.R;
import my.dagger.daggersample.utils.BaseApplication;

public class MyDaggerActivity extends AppCompatActivity {

    @Inject
    DatabaseHandler db;

    @Inject
    StudentModel studentModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dagger);

        BaseApplication.myComponent.inject(this);

        db.connect();
        studentModel.name = "mona"; 

    }
}
