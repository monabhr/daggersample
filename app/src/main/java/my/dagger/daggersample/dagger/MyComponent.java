package my.dagger.daggersample.dagger;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {MyModule.class})
@Singleton
public interface MyComponent {

   void inject(MyDaggerActivity myDaggerActivity);


}
