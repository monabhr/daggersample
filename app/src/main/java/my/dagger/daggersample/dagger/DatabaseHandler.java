package my.dagger.daggersample.dagger;

import android.content.Context;

public class DatabaseHandler {

    String name, address, password;
    Context mContext;

    public DatabaseHandler(String name, String address, String password) {
        this.name = name;
        this.address = address;
        this.password = password;
    }

    public DatabaseHandler(Context mContext) {
        this.mContext = mContext;
    }

    public void  connect(){
        //write code to connect to db
    }

    public void insert(String query){
        //code for inserting in db
    }



}
