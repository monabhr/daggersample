package my.dagger.daggersample.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MyModule {

    Context mContext;

    public MyModule(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    @Singleton
    public Context getContext() {
        return mContext;
    }

    @Provides
    @Singleton
    public DatabaseHandler getDatabase(Context mContext) {
        return new DatabaseHandler(mContext);
    }

    @Provides
    @Singleton
    public StudentModel getStudent(DatabaseHandler databaseHandler) {
        return new StudentModel(databaseHandler);
    }
}
